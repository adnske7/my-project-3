'use strict'

/**
 * Dependencies
 * @ignore
 */

 const express = require("express");
 const morgan = require("morgan");

 /**
  * App
  * @ignore
  */
 const app = express();

 app.use(morgan("tiny"))
 app.use(express.static("dist"))

 //listen
 app.listen(process.env.PORT||3000,() =>
 console.log('Listening on port $(process.env.PORT || 3000)')
 ); 