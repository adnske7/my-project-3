import Vue from 'vue';
import App from './App.vue'; 
import VueRouter from 'vue-router';
import GameOver from './components/GameOver.vue';
import GameMenu from './components/GameMenu.vue';
import GamePlay from './components/GamePlay.vue';

Vue.use(VueRouter);

const routes = [
  {path: '/GameMenu', component: GameMenu},
  {path: '/GamePlay', component: GamePlay},
  {path: '/GameOver', component: GameOver}
];

  const router = new VueRouter({
  routes,
  mode: 'history'
});

Vue.config.productionTip = false

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
